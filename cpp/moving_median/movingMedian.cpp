#include <iostream>
#include <queue>
#include <set>
#include <cstdlib>

using namespace std;

// g++ -std=c++20 -O3 medianFilter.cpp -o medianFilter && time ./medianFilter

template <typename T>
struct MedianFilter
{
    static const unsigned int N {9};
    queue<T> sequential_order; // Would prefer to make this fixed size
    multiset<T> hi;
    multiset<T> lo;
    
    MedianFilter();
    T update(T u);
};

template <typename T>
MedianFilter<T>::MedianFilter()
{
    while (sequential_order.size() < N)
    {
        sequential_order.push(0.0);
    }
    while (lo.size() < N/2)
    {
        lo.insert(0.0);
        hi.insert(0.0);
    }
    if ((lo.size() + hi.size()) < N)
    {
        lo.insert(0.0);
    }
}

template <typename T>
T MedianFilter<T>::update(T u)
{
    auto oldest = sequential_order.front();
    sequential_order.pop();
    sequential_order.push(u);

    if (oldest != u)
    {
        if (oldest >= *hi.begin()) // o is in hi
        {
            if (u > *(--lo.end())) // u should go in hi
            {
                hi.erase(hi.lower_bound(oldest)); // O(log(n))
                hi.insert(u); // O(log(n))
            }
            else // in lo
            {
                hi.erase(hi.lower_bound(oldest)); // O(log(n))
                hi.insert(hi.begin(), *(--lo.end())); // O(1)
                lo.erase(--lo.end()); // O(1)
                lo.insert(u); // O(log(n))
            }
        }
        else // o is in lo (it better be)
        {
            if (u < *hi.begin()) // u should go in lo
            {
                lo.erase(lo.lower_bound(oldest)); // O(log(n))
                lo.insert(u); // O(log(n))
            }
            else // u should go in hi
            {
                lo.erase(lo.lower_bound(oldest)); // O(log(n))
                lo.insert(*hi.begin()); // O(log(n))
                hi.erase(hi.begin()); // O(1)
                hi.insert(u); // O(log(n))
            }
        }
    }
    if (N%2 > 0) // Make this polymorphic, either compile or construction
    {
        return *(--lo.end());
    }
    else
    {
        // Need to figure out what to do for ints...
        return (*(--lo.end()) + *hi.begin())/2.0;
    }
}

int main(int argc, char *argv[])
{
    std::srand(0);
    std::cout << "argc == " << argc << '\n';
 
    MedianFilter<double> median_filter {};

    
    uint32_t i = 0;
    double u = 0.0;

    while (1)
    {
        u = static_cast<double>(std::rand()) / RAND_MAX;
        cout << "Before lo : hi ==> ";
        for (auto t : median_filter.lo)
            cout << t << " ";
        cout << ": ";
        for (auto t : median_filter.hi)
            cout << t << " ";
        cout << endl;
        cout << "Count: " << i++ << ", ";
        cout << "Front: " << median_filter.sequential_order.front() << " : ";
        cout << "New: " << u << ", ";
        cout << "New Median: " << median_filter.update(u) << endl;
        cout << "After lo : hi ==> ";
        for (auto t : median_filter.lo)
            cout << t << " ";
        cout << ": ";
        for (auto t : median_filter.hi)
            cout << t << " ";
        if (i > 20)
            break;
        cout << endl;
        cout << endl;        
    }

    return argc == 3 ? EXIT_SUCCESS : EXIT_FAILURE; // optional return value
}



