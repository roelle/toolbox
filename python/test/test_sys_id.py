import unittest
import logging
import sys
import numpy as np

from toolbox import sys_id

logger = logging.getLogger(__name__)

class PRBSTest(unittest.TestCase):

    def test_update_vs_generate(self):
        n = np.random.randint(2,16)
        prbs = sys_id.PRBS(n)
        u = sys_id.PRBS.generate(seed=prbs.state)
        self.assertEqual(prbs.nonrepeating_sequence_length, prbs.state.size)
        self.assertEqual(prbs.sequence_length, 2**n - 1)
        self.assertEqual(u.size, 2**n - 1)
        for i, s in enumerate(prbs.state):
            self.assertEqual(s, u[i])
        for i in range(i+1, prbs.sequence_length):
            prbs.update()
            self.assertEqual(prbs.output, u[i])

    def test_completeness(self):
        for nrs_length in range(1, 17):
            seed = np.round(np.random.random(nrs_length)).astype(np.int8)
            if sum(seed) == 0:
                seed[0] = 1
            logging.debug(f'Testing seed: {seed}')
            prbs = sys_id.PRBS(seed=seed)
            checks = np.zeros(prbs.sequence_length + 1, dtype=np.int_)
            for i in range(prbs.sequence_length):                       
                index = int(
                    prbs.state.dot(
                        1 << np.arange(prbs.state.size)[::-1]
                    )
                )
                checks[index] += 1
                prbs.update()
            self.assertEqual(
                checks[0],
                0,
                f'All-zero non-repeating sequence not allowed, seed: {seed}',
            )
            self.assertTrue(
                np.all(checks[1:])==1,
                f'Completeness failure for seed: {seed}',
            )

#################################
### Module Level Test Harness ###
#################################
if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    # Kick off all unit tests contained in the file
    unittest.main()
