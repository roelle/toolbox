#!/usr/bin/env python

from distutils.core import setup

setup(
    name='toolbox',
    version='0.0',
    description='Personal math and controls toolbox',
    author='Matt Roelle',
    author_email='roelle@gmail.com',
    url='https://gitlab.com/roelle/toolbox/',
    packages=['toolbox'],
    # package_dir={'toolbox': 'src/toolbox'},
    python_requires="==3.10.*",
    install_requires=[
        "numpy>=1.*",
    ]
)