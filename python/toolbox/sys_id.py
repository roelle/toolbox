import numpy as np

class PRBS():
    """Psuedo-random binary sequence

    The psuedo-random binary sequence (PRBS) is composed of non-repeating
    sub-sequences. This gives the overal sequence favorable properties for
    system identification or other signal-processing. Much more can be found
    online e.g. Wikipedia.

    From Ljung, a pseudo-random binary signal is a periodic, 
    deterministic signal with white noise like properties. The PRBS has 
    optimal crest factor and may also be called a "gold code."

    Reference Ljung, Lennart; System Identification, Theory for the User,
    2nd Ed.,Prentice Hall, pp. 418-422


    Attributes
    ----------
    nonrepeating_sequence_length : int
        length of the sub-sequence that does not repeat
    sequence_length : int
        total length of the binary sequence
    state : numpy.ndarray
        current sub-sequence
    output : int
        latest binary value

    Methods
    -------
    update() :
        shift the PRBS forward one point (get it with output)
    generate(nonrepeating_sequence_length: int = None, seed: list = None) :
        create a full-length PRBS
    next_point(state) :
        given a binary sub-sequence, generate the next point of a PRBS
    maximum_sequence_length() : int
        calculate the total length of a PRBS 

    Typical Usage
    -------------
    from toolbox.sys_id import PRBS
    # Make an entire sequence
    u = PRBS.generate(n := 5, np.round(np.random.random(n)))

    or 

    # Get one point at a time
    x = [0, 1, 0, 1, 0]
    while True:
        x.append(PRBS.next_point(np.array(x)))
        x.pop(0)
        print(x[-1]) # Emit the latest point

    or

    # Also one point at a time, but letting the object manage the state
    prbs = PRBS(8)
    while True:
        prbs.update()
        print(prbs.output)
    """

    TYPE=np.int16
    P = [ # Feedback polynomial
        np.array([], dtype=TYPE),
        np.array([1], dtype=TYPE),
        np.array([1, 1], dtype=TYPE),
        np.array([1, 1, 0], dtype=TYPE),
        np.array([1, 0, 0, 1], dtype=TYPE),
        np.array([1, 0, 0, 1, 0], dtype=TYPE),
        np.array([1, 1, 0, 0, 0, 0], dtype=TYPE),
        np.array([1, 0, 0, 0, 1, 0, 0], dtype=TYPE),
        np.array([1, 1, 0, 0, 0, 0, 1, 1], dtype=TYPE),
        np.array([1, 0, 0, 0, 0, 1, 0, 0, 0], dtype=TYPE),
        np.array([1, 0, 0, 1, 0, 0, 0, 0, 0, 0], dtype=TYPE),
        np.array([1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0], dtype=TYPE),
        np.array([1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1], dtype=TYPE),
        np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1], dtype=TYPE),
        np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1], dtype=TYPE),
        np.array([1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=TYPE),
        np.array([1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0], dtype=TYPE),
    ]

    def __init__(
        self,
        nonrepeating_sequence_length: int = None,
        seed: list = None,
        sequence_length: int = None,
    ):
        '''Generate a pseudo-random binary sequece (PRBS)'''
        # Figure out the non-repeating sequence length
        if nonrepeating_sequence_length is None:
            if seed is not None:
                nonrepeating_sequence_length = len(seed)
            elif sequence_length is not None:
                # Assume repetition of a complete sequence rather than a
                # partial sequence
                nonrepeating_sequence_length = np.floor(
                    np.log(sequence_length + 1, 2)
                )
            else:
                raise(NameError('Must provide base or total sequence length'))
        
        # If a sequence length was not provided, just use the maximum sequence
        # length
        if sequence_length is not None:
            self.sequence_length = sequence_length
        else:
            self.sequence_length = self.maximum_sequence_length(
                nonrepeating_sequence_length
            )

        # Generate a default seed or make sure it is an array (not a list) 
        if seed is None:
            self.state = self.default_seed(nonrepeating_sequence_length)
        else:
            self.state = np.array(seed)

    def update(self):
        '''Generate the next point in the PRBS sequence'''
        self.state = np.hstack((self.state[1:], self.next_point(self.state)))

    @property
    def output(self):
        '''Retrieve the latest point in the PRBS sequence'''
        return self.state[-1]

    @property
    def nonrepeating_sequence_length(self):
        '''Return the non-repeating sequence length for the prbs'''
        return self.state.size

    @staticmethod
    def maximum_sequence_length(n: int):
        '''Calculate the sequence length for a given non-repeating sequence'''
        return 2**n - 1

    @staticmethod
    def next_point(seed):
        '''Calculate the next point in a PRBS

        This method is where the real magic of the PRBS lives. It could be
        iterated without even using any other functions. For example:
        ```python
        x = [0, 1, 0, 1, 0]
        while True:
            x.append(PRBS.next_point(np.array(x)))
            x.pop(0)
            print(x[-1]) # Emit the latest point
        ``` 
        '''
        return np.remainder(np.dot(PRBS.P[seed.size], seed), 2)

    @staticmethod
    def default_seed(nonrepeating_sequence_length):
        '''Generate a default seed'''
        return np.hstack(
            (
                np.zeros(nonrepeating_sequence_length-1, dtype=PRBS.TYPE),
                1,
            )
        )

    @staticmethod
    def generate(nonrepeating_sequence_length: int = None, seed: list = None):
        """Return a complete psuedo-random binary sequence"""
        if nonrepeating_sequence_length is None and seed is not None:
            n = len(seed)
        else:
            n = nonrepeating_sequence_length
    
        if seed is None:
            seed = PRBS.default_seed(n)
        else:
            seed = seed[0:n+1]

        u = np.zeros(PRBS.maximum_sequence_length(n), dtype=PRBS.TYPE)
        u[0:n] = seed
        for k in range(n, PRBS.maximum_sequence_length(n)):
            u[k] = PRBS.next_point(u[k-n:k])
        return u


#################################
### Module Level Test Harness ###
#################################
if __name__ == "__main__":
    # logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    # Kick off all unit tests contained in the file
    pass
